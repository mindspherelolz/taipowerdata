FROM alpine
FROM python:3.7

RUN pip3 install --upgrade pip

WORKDIR /TaiPower

COPY . /TaiPower

RUN pip3 --no-cache-dir install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["TaiPower.py"]