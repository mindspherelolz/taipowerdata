# -*- coding: UTF-8 -*-
import requests
from bs4 import BeautifulSoup
import json
import pandas as pd
from flask import Flask, request, redirect, send_from_directory, url_for
import os
from flask_cors import CORS
import re
from flask_pymongo import PyMongo
from pymongo import MongoClient

app = Flask(__name__)
# client = MongoClient("mongodb://nick:nick123@ds017776.mlab.com:17776/testing?retryWrites=false") #host uri
# db = client.mymongodb #Select the database
# test = db.test
app.config["MONGO_URI"] = "mongodb://nick:nick123@ds017776.mlab.com:17776/testing?retryWrites=false"
CORS(app)
mongo = PyMongo(app)

@app.route("/", methods=['GET'])
def generateFile():
    # file = appendCSV(filename="power")
    file = 'test'
    return file
    # return redirect("/"+file)
    # return send_from_directory('.', 'all.csv', as_attachment=True)

@app.route("/mongo",methods=['GET'])
def testingMongo():
    r_json = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/data/genary.txt')
    r_json.encoding = 'utf-8'
    j = json.loads(r_json.text)
    # test.insert({"test" : 123})
    mongo.db.test.insert(j)
    return "Successfully"
    # return mongo.send_file(filename)

# @app.route("/uploads/<path:filename>", methods=["POST"])
# def save_upload(filename):
#     mongo.save_file(filename, request.files["file"])
#     return redirect(url_for("get_upload", filename=filename))

@app.route("/generate", methods=['GET'])
def generate():
    # file = appendCSV(filename="power")
    # return redirect("/"+file)
    file = generateCSV(filename="power")
    return redirect("/"+file)

@app.route("/<string:filename>", methods=['GET'])
def download(filename):
    return send_from_directory('.', filename, as_attachment=True)

@app.route("/testing", methods=['GET'])
def testing(filename="power"):
    r = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/genshx_.html')
    r.encoding = 'utf-8'
    table = BeautifulSoup(r.text, 'html.parser').find('table', class_='display')
    columns = [c.text for c in table.find('thead').find_all('th')]

    r_json = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/data/genary.txt')
    r_json.encoding = 'utf-8'
    j = json.loads(r_json.text)
    time = j[''].replace(':', '')
    data = j['aaData']
    i = 0 
    for x in data:
        temp = x[0].split("(")[0]
        temp = temp.split(">")[3]
        temp2 = x[1].split("amp;")
        string = ''
        for z in temp2:
            string += z
        temp2 = string
        print(temp2)
        data[i][0] = temp
        data[i][1] = temp2
        i = i + 1
    df = pd.DataFrame(data, columns=columns)
    date = time.split()[0]
    filename = filename + '_'+date+'.csv'
    return "ok"

@app.route("/append", methods=['GET'])
def appendCSV(filename="power"):
    r = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/genshx_.html')
    r.encoding = 'utf-8'
    table = BeautifulSoup(r.text, 'html.parser').find('table', class_='display')
    columns = [c.text for c in table.find('thead').find_all('th')]

    # r_json = requests.get('http://data.taipower.com.tw/opendata01/apply/file/d006001/001.txt')
    r_json = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/data/genary.txt')
    r_json.encoding = 'utf-8'
    j = json.loads(r_json.text)
    time = j[''].replace(':', '')
    data = j['aaData']
    i = 0 
    for x in data:
        temp = x[0].split("(")[0]
        temp = temp.split(">")[3]
        temp2 = x[1].split("amp;")
        string = ''
        for z in temp2:
            string += z
        temp2 = string
        data[i][0] = temp
        data[i][1] = temp2
        i = i + 1
    df = pd.DataFrame(data, columns=columns)
    date = time.split()[0]
    remarks = '備註' + time.split()[1]
    filename = filename + '_'+date+'.csv'
    # filename = filename + '_'+time+'.csv'
    if not os.path.isfile(filename):
        df_header = df[['燃料別','機組名稱']]
        df_header.to_csv(filename)
        df = df_header
        df_newdata = pd.DataFrame([d[3] for d in data], columns=[time])
        df = pd.concat([df,df_newdata],axis=1)
        df_newdata2 = pd.DataFrame([d[5] for d in data], columns=[remarks])
        df = pd.concat([df,df_newdata2],axis=1)
        df.to_csv(filename, index=False, encoding='utf_8_sig')
    else:
        df = pd.read_csv(filename)
        if time in df.columns:
            return filename
        df_newdata = pd.DataFrame([d[3] for d in data], columns=[time])
        df = pd.concat([df,df_newdata], axis=1)
        df_newdata2 = pd.DataFrame([d[5] for d in data], columns=[remarks])
        df = pd.concat([df,df_newdata2],axis=1)
        df.to_csv(filename, index=False, encoding='utf_8_sig')
    return filename

def generateCSV(filename="power"):
    print("Generating new data..")
    r = requests.get('https://www.taipower.com.tw/d006/loadGraph/loadGraph/genshx_.html')
    r.encoding = 'utf-8'
    table = BeautifulSoup(r.text, 'html.parser').find('table', class_='display')
    columns = [c.text for c in table.find('thead').find_all('th')]

    r_json = requests.get('http://data.taipower.com.tw/opendata01/apply/file/d006001/001.txt')
    r_json.encoding = 'utf-8'
    j = json.loads(r_json.text)
    time = j[''].replace(':', '')
    data = j['aaData']

    df = pd.DataFrame(data, columns=columns)
    df.name = time
    df.to_csv(filename+'_'+time+'.csv', encoding='utf_8_sig')
    print("Finished")
    return filename+'_'+time+'.csv'

# port = int(os.getenv('PORT',9099))

cf_port = os.getenv("PORT")

if __name__ == '__main__':
    if cf_port is None:
        app.run(host='0.0.0.0', port=5000, debug=True)
    else:
        app.run(host='0.0.0.0', port=int(cf_port), debug=False)
    # app.run(debug=True, port=port)
